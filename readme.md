# API
***
This project is an API made with ApiPlatform which allows an Mandataire to transmit the data of his cars for sale.
***
***
## Status
> Project development is complete.
***
***
## The technical requirements
***
* Install PHP 7.2.5 or higher.
* Install composer which is used to install PHP packages.
* Install git to retrieve the project.
***
***
## Documentation
[install PHP](https://www.php.net/manual/fr/install.php)
[Symfony](https://symfony.com/)
[ApiPlatform](https://api-platform.com/docs/core/getting-started/)
[DoctrineFixtures](https://symfony.com/doc/current/the-fast-track/en/17-tests.html#writing-functional-tests-for-controllers)
[VichUploaderBundle](https://symfony.com/bundles/EasyAdminBundle/2.x/integration/vichuploaderbundle.html#uploading-other-types-of-files)
[lexik/jwt-authentication](https://github.com/lexik/LexikJWTAuthenticationBundle)
[mailhog](https://kinsta.com/fr/blog/mailhog/)
***
***
## Install the project
***
1. [Go to this remote repository](https://gitlab.com/davidjeje/apimandataire)
2. __Copy this address to the command line__ with SSH: git clone git@gitlab.com:davidjeje/apimandataire.git or with HTPPS: git clone https://gitlab.com/davidjeje/apimandataire.git
***
***
## Bundles
***
* lexik/jwt-authentication.
* gesdinet/jwt-refresh-token.
* symfonycasts/verify-email.
* VichUploaderBundle.
* fakerphp.
***
***
## Command line
__For PHP and composer:__
* Then quickly check that PHP and Composer are available in your command prompt with php -v and composer --version.
***
__For internal server:__ 
* To start the internal server the online command is: symfony serve -d
* To end the internal server the online command is: symfony server:stop
***
__For data base:__ 
* To create a database the online command is: php bin/console doctrine:database:create
* To generate a new migration the command line is: php bin/console make:migration
* To run the migration the command line is: php bin/console doctrine:migrations:migrate
***
__For data base test:__ 
* symfony console doctrine:database:create --env=test
* symfony console  doctrine:schema:create --env=test
* symfony composer req orm-fixtures --dev
* symfony console doctrine:fixtures:load --env=test
***
__For data fixtures test:__ 
* Once the fixtures are written here is the command to load the data: php bin/console doctrine:fixtures:load
***
__For test email:__ 
* To test sending email locally with windows, configure your MAILER_DNS which is in your .env file by putting this: MAILER_DSN=smtp://localhost:1025
* Secondly, go to your browser and add port 8025 like this: http://127.0.0.1:8025/
***
***
## Author of the project
__David Marivat__