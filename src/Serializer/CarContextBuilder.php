<?php

namespace App\Serializer;

use App\Entity\Car;
use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CarContextBuilder implements SerializerContextBuilderInterface
{
    private $decorated;
    private $authorizationCheckerInterface;

    public function __construct(SerializerContextBuilderInterface $decorated, AuthorizationCheckerInterface $authorizationCheckerInterface)
    { 
        $this->decorated = $decorated;
        $this->authorizationCheckerInterface = $authorizationCheckerInterface;
    } 

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $ressourceClass = $context['resource_class'] ?? null;
        
        if(
            $ressourceClass === Car::class &&
            isset($context['groups']) &&
            $this->authorizationCheckerInterface->isGranted('ROLE_ADMIN')
        ){
            $context['groups'][] = 'car:read:admin';
        }
        return $context;
    }
}