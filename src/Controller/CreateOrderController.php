<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Order;
use Symfony\Component\Security\Core\Security;

class CreateOrderController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function __invoke(Order $data)
    {
        $data->setUser($this->security->getUser());

        return $data;
    }  
}