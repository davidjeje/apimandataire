<?php

namespace App\Controller\Api;

use App\Entity\User;

class DeactivateUser
{
    public function __invoke(User $data)
    {
        $user = $data;

        $user->setRoles(['ROLE_ANONYME']);
        $user->setIsVerified(false);
        
        return $user;
    }
}
