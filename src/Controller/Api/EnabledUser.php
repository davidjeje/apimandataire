<?php

namespace App\Controller\Api;

use App\Entity\User;

class EnabledUser
{
    public function __invoke(User $data)
    {
        $user = $data;

        $user->setRoles(['ROLE_USER']);
        $user->setIsVerified(true);

        return $user;
    }
}
