<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/api/cookie/login", name = "api_cookie_login", methods = {"POST"})
     */
    public function apiCookieLogin(): Response
    {
        $user = $this->getUser();
        return $this->json([
            "username" => $user->getEmail(),
            "roles" => $user->getRoles()
        ]);        
    }

    /**
     * @Route("/api/login", name = "api_login", methods = {"POST"})
     */
    public function apiLogin(): Response
    {
        $user = $this->getUser();
        return $this->json([
            "username" => $user->getEmail(),
            "roles" => $user->getRoles()
        ]);        
    }

    /**
     * @Route("/api/logout", name="app_logout", methods = {"GET|POST"})
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
}
