<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Car;
use Symfony\Component\Security\Core\Security;

class CreateCarController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function __invoke(Car $data)
    {
        $data->setUser($this->security->getUser());

        return $data;
    }  
}