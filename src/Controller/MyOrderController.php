<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; 
use Symfony\Component\Security\Core\Security;
use App\Repository\OrderRepository;

class MyOrderController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function __invoke(OrderRepository $orderRepository)
    {
        //dd($orderRepository->findAll());
        $user = $this->security->getUser();
        $orders = $orderRepository->findBy(['user' => $user->getId()]); 
        
        return $orders;
    } 
}
