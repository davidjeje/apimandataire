<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Car;
use RuntimeException;

class CarImageController extends AbstractController
{

    public function __invoke(Request $request)
    {
        $car = $request->attributes->get('data'); 

        if(!($car instanceof Car)){
            throw new RuntimeException('Voiture attendu !!!');
        }

        $car->setImageFile($request->files->get('file'));
        return $car;
    }  
}