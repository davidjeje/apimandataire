<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
 
class CreateUserController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function __invoke(User $data)
    {
        $password = $this->encoder->hashPassword($data, $data->getPassword());
        $data->setPassword($password);

        return $data;
    }  
}