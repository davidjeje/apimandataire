<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use Symfony\Flex\Path;

class OpenApiFactory implements OpenApiFactoryInterface
{
    private $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);
        
        /** @var PathItem $path */
        foreach($openApi->getPaths()->getPaths() as $key => $path){ 
            if($path->getGet() && $path->getGet()->getSummary() === 'hidden'){
                $openApi->getPaths()->addPath($key, $path->withGet(null));
            }
        }

        $schemas = $openApi->getComponents()->getSecuritySchemes();
        $schemas["cookieAuth"] = new \ArrayObject([
            "type" =>"apiKey",
            "in" => "cookie",
            "name" => "PHPSESSID"
        ]);

        $schemas = $openApi->getComponents()->getSecuritySchemes();
        $schemas["bearerAuth"] = new \ArrayObject([
            "type" =>"http",
            "scheme" => "bearer",
            "bearerFormat" => "JWT"
        ]);

        $schemas = $openApi->getComponents()->getSchemas(); 

        $schemas['Credentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'onlineUser@gmail.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'online',
                ]
            ]
        ]);

        $schemas['Token'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true
                ],
            ],
        ]); 
        
        $meCookieOperation = $openApi->getPaths()->getPath('/api/me/cookie')->getGet()->withParameters([]);
        $meCookiePathItem = $openApi->getPaths()->getPath('/api/me/cookie')->withGet($meCookieOperation);
        $openApi->getPaths()->addPath('/api/me/cookie', $meCookiePathItem);


        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiLogin',
                tags: ['Auth cookie'],
                requestBody: new RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials'
                            ]
                        ]
                    ])
                ),
                responses: [
                    '200' => [
                        'description' => ' Admin connecté',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/User-user.read'
                                ]
                            ]
                        ]
                    ]
                ]
            )
        );
        $openApi->getPaths()->addPath('/api/cookie/login', $pathItem);

        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiLogin',
                tags: ['Auth jwt'],
                requestBody: new RequestBody(
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials'
                            ]
                        ]
                    ])
                ),
                responses: [
                    '200' => [
                        'description' => 'Token JWT',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token'
                                ]
                            ]
                        ]
                    ]
                ]
            )
        );
        $openApi->getPaths()->addPath('/api/login', $pathItem);

        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiLogout',
                tags: ['Logout'],
                responses: [
                    '204' => []
                ]
            )
        );
        $openApi->getPaths()->addPath('/api/logout', $pathItem);

        return $openApi;
    }
}
