<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface; 
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Action\NotFoundAction;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Api\FilterInterface; 

/**
 * @ApiResource(
 *      attributes = {
 *          "pagination_items_per_page" = 10,
 *          "security" = "is_granted('ROLE_ADMIN')"
 *      },
 *      normalizationContext={"groups"={"user:read"}},
 *      denormalizationContext={"groups"={"user:write"}},
 *      collectionOperations={
 *          "get"={
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              }
 *          },
 *          "post"={
 *              "controller" = ApiPlatform\Core\Action\NotFoundAction::class,
 *              "openapi_context" = {"summary" = "hidden"},
 *              "read" = false,
 *              "output" = false,
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              }
 *          },
 *          "create_user"={
 *              "validation_groups" = {"validation"},
 *              "method"="POST",
 *              "path"="/users/create",
 *              "controller"=App\Controller\CreateUserController::class,
 *              "denormalization_context" = {"groups"={"user:write", "admin:write"}},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {} }},
 *                  "summary" = "Permet à l'administrateur d'enregistrer un utilisateur avec un mot de passe encodé.",
 *                  "requestBody" = {
 *                      "content" = {
 *                          "application/json" = {
 *                              "schema" = {
 *                                  "type" = "object",
 *                                  "properties" = {
 *                                      "email" = {"type" = "string"},
 *                                      "roles" = {"type" = "string"},
 *                                      "password" = {"type" = "string"},
 *                                      "name" = {"type" = "string"},
 *                                      "number" = {"type" = "string"},
 *                                      "adress" = {"type" = "string"},
 *                                      "city" = {"type" = "string"},
 *                                      "zipCode" = {"type" = "string"},
 *                                      "isVerified" = {"type" = "boolean"},
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *      },
 *      itemOperations={
 *          "get"={
 *              "normalization_context" = {"groups"={"user:read", "user:item"}},
 *              "security" = "is_granted('USER_LOGIN', object)",
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              },
 *          },
 *          "me_cookie"={
 *              "normalization_context" = {"groups"={"user:read", "user:me"}},
 *              "pagination_enabled" = false,
 *              "path" = "/me/cookie",
 *              "method" ="GET",
 *              "controller" = App\Controller\MeController::class,
 *              "read" = false,
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              },
 *              "security" = "is_granted('ROLE_USER')"
 *          },
 *          "enabled_user"={
 *              "method"="PATCH",
 *              "path"="/user/change/{id}/enabled",
 *              "controller"=App\Controller\Api\EnabledUser::class,
 *              "validate" = false,
 *              "normalization_context" = {"groups"={"user:read", "user:isverified"}},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}},
 *                  "summary" = "Permet de valider le compte d'une entreprise.",
 *                  "requestBody" = {
 *                      "content" = {
 *                          "application/merge-patch+json" = {
 *                              "schema" = {}
 *                          }
 *                      }
 *                  }
 *              },
 *              "security" = "is_granted('ROLE_ADMIN')"
 *          },
 *          "deactivate_user"={
 *              "method"="PATCH",
 *              "path"="/user/change/{id}/deactivate",
 *              "controller"=App\Controller\Api\DeactivateUser::class,
 *              "validate" = false,
 *              "normalization_context" = {"groups"={"user:read", "user:isverified"}},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}},
 *                  "summary" = "Permet de désactiver le compte d'une entreprise.",
 *                  "requestBody" = {
 *                      "content" = {
 *                          "application/merge-patch+json" = {
 *                              "schema" = {}
 *                          }
 *                      }
 *                  }
 *              }
 *          }, 
 *          "put"={
 *              "denormalization_context" = {"groups"={"user:put"}},
 *              "validation_groups" = {"validation"},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              },
 *              
 *          },
 *          "patch"={
 *              "denormalization_context" = {"groups"={"user:put"}},
 *              "validation_groups" = {"validation"},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              },
 *              "security" = "is_granted('USER_LOGIN', object)",
 *          },
 *          "delete"={
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              },
 *          },
 *      }
 * )
 * @ApiFilter(BooleanFilter::class, properties = {"isVerified"})
 * @ApiFilter(SearchFilter::class, properties = {"roles" = "exact"})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface//, JWTUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:write"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     groups={"validation"}
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user:item", "admin:write"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user:write", "user:put"})
     * @Assert\NotBlank(groups={"validation"})
     * @Assert\NotNull(groups={"validation"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read", "user:write", "user:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 2,
     *     max = 15,
     *     minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *     maxMessage = "This value is too long. It should have {{ limit }} characters or less.",
     *     groups={"validation"}
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:item", "user:write", "user:put", "user:me"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 10,
     *     max = 10,
     *     minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *     maxMessage = "This value is too long. It should have {{ limit }} characters or less.",
     *     groups={"validation"}
     * )
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:item", "user:write", "user:put", "user:me"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 10,
     *     max = 40,
     *     minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *     maxMessage = "This value is too long. It should have {{ limit }} characters or less.",
     *     groups={"validation"}
     * )
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:item", "user:write", "user:put", "user:me"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 3,
     *     max = 20,
     *     minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *     maxMessage = "This value is too long. It should have {{ limit }} characters or less.",
     *     groups={"validation"}
     * )
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:item", "user:write", "user:put", "user:me"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 5,
     *     max = 5,
     *     minMessage = "This value is too short. It should have {{ limit }} characters or more.",
     *     maxMessage = "This value is too long. It should have {{ limit }} characters or less.",
     *     groups={"validation"}
     * )
     */
    private $zipCode;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     * @Groups({"user:item", "user:isverified", "admin:write"})
     */
    private $isVerified = 0;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $orderId;

    /**
     * @ORM\OneToMany(targetEntity=Car::class, mappedBy="user")
     */
    private $car;

    public function __construct()
    {
        $this->orderId = new ArrayCollection();
        $this->car = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = NULL/*'ROLE_USER'*/;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    } 

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /*public static function createFromPayload($username, array $payload)
    {
        /*dd($username, $payload);
        $user = New Admin();
        $user->setEmail($username);
        dd($user);
        return $user;*/
        /*return new self(
            $username,
            $payload['roles'], // Added by default
            $payload['username']  // Custom
        );
    }*/

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrderId(): Collection
    {
        return $this->orderId;
    }

    public function addOrderId(Order $orderId): self
    {
        if (!$this->orderId->contains($orderId)) {
            $this->orderId[] = $orderId;
            $orderId->setUser($this);
        }

        return $this;
    }

    public function removeOrderId(Order $orderId): self
    {
        if ($this->orderId->removeElement($orderId)) {
            // set the owning side to null (unless already changed)
            if ($orderId->getUser() === $this) {
                $orderId->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCar(): Collection
    {
        return $this->car;
    }

    public function addCar(Car $car): self
    {
        if (!$this->car->contains($car)) {
            $this->car[] = $car;
            $car->setUser($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->car->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getUser() === $this) {
                $car->setUser(null);
            }
        }

        return $this;
    }
}
