<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\Date;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
 

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 * @Vich\Uploadable()
 * @ApiResource(
 *      attributes = {
 *          "pagination_items_per_page" = 10,
 *          "pagination_client_items_per_page" = true,
 *          "pagination_maximum_items_per_page" = 10, 
 *          "security" = "is_granted('ROLE_ADMIN')"
 *      },
 *      normalizationContext={"groups"={"car:read"}},
 *      denormalizationContext={"groups"={"car:write"}},
 *      collectionOperations={
 *          "get"={
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}}
 *              },
 *              "security" = "is_granted('ROLE_USER')"
 *          },
 *          "post"={
 *              "controller" = ApiPlatform\Core\Action\NotFoundAction::class,
 *              "openapi_context" = {"summary" = "hidden"},
 *              "read" = false,
 *              "output" = false,
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {}}} 
 *              }
 *          },
 *          "create_car"={
 *              "validation_groups" = {"validation"},
 *              "method"="POST",
 *              "path"="/cars/create",
 *              "controller"=App\Controller\CreateCarController::class,
 *              "normalization_context" = {"groups"={"car:read", "user:create"}},
 *              "openapi_context" = {
 *                  "security" = {{"bearerAuth" = {} }},
 *                  "summary" = "Permet d'attribuer la création d'une voiture à un Administrateur.",
 *                  "requestBody" = {
 *                      "content" = {
 *                          "application/json" = {
 *                              "schema" = {
 *                                  "type" = "object",
 *                                  "properties" = {
 *                                      "mark" = {"type" = "string"},
 *                                      "model" = {"type" = "string"},
 *                                      "finishing" = {"type" = "string"},
 *                                      "fuel" = {"type" = "string"},
 *                                      "gearbox" = {"type" = "string"},
 *                                      "description" = {"type" = "string"},
 *                                      "newCar" = {"type" = "boolean"},
 *                                      "yearCar" = {"type" = "string"},
 *                                      "mileage" = {"type" = "string"},
 *                                      "color" = {"type" = "string"},
 *                                      "price" = {"type" = "string"},
 *                                      "stock" = {"type" = "string"},
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *      },
 *      itemOperations={
 *          "get"={
 *              "normalization_context" = {"groups"={"car:read", "car:item"}},
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"bearerAuth" = {}}
 *                  }
 *              },
 *              "security" = "is_granted('ROLE_USER')"
 *          },
 *          "put"={
 *              "denormalization_context" = {"groups"={"car:put"}},
 *              "validation_groups" = {"validation"},
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"bearerAuth" = {}},
 *                  }
 *              },
 *              
 *          },
 *          "patch"={
 *              "denormalization_context" = {"groups"={"car:put"}},
 *              "validation_groups" = {"validation"},
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"bearerAuth" = {}}
 *                  },
 *              },
 *          },
 *          "delete"={
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"bearerAuth" = {}}
 *                  }
 *              },
 *          },
 *          "image"={
 *              "path" = "/cars/{id}/image",
 *              "method" ="POST",
 *              "deserialize" = false,
 *              "controller" = App\Controller\CarImageController::class,
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"bearerAuth" = {}}  
 *                  },
 *                  "requestBody" = {
 *                      "content" = {
 *                          "multipart/form-data" = {
 *                              "schema" = {
 *                                  "type" = "object",
 *                                  "properties" = {
 *                                      "file" = {
 *                                          "type" = "string",
 *                                          "format" = "binary"
 *                                       }
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties = {"mark" = "exact", "fuel" = "exact", "gearbox" = "exact"})
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"car:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:read", "car:write"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 50,
     *     groups={"validation"}
     * )
     */
    private $mark;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:read", "car:write"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     groups={"validation"}
     * )
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 5,
     *     max = 255,
     *     groups={"validation"}
     * )
     */
    private $finishing;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put", "car:read:admin"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 6,
     *     max = 15,
     *     groups={"validation"}
     * )
     */
    private $fuel;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put", "car:read:admin"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 4,
     *     max = 20,
     *     groups={"validation"}
     * )
     */
    private $gearbox;

    /**
     * @ORM\Column(type="text")
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 5,
     *     max = 100000,
     *     groups={"validation"}
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull(groups={"validation"})
     */
    private $newCar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 4,
     *     max = 10,
     *     groups={"validation"}
     * )
     */
    private $yearCar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 10,
     *     groups={"validation"}
     * )
     */
    private $mileage;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     *     groups={"validation"}
     * )
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 10,
     *     groups={"validation"}
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"car:item", "car:write", "car:put"})
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Length(
     *     min = 1,
     *     max = 10,
     *     groups={"validation"}
     * )
     */
    private $stock;

    /**
     * @ORM\ManyToMany(targetEntity=Order::class, mappedBy="carsId")
     */
    private $orderId;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="car")
     * @Groups({"user:create"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @Vich\UploadableField(mapping="car_images", fileNameProperty="filePath")
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Groups({"car:item", "car:read"})
     * @var string|null
     */
    private $fileUrl;

    public function __construct()
    {
        $this->orderId = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMark(): ?string
    {
        return $this->mark;
    }

    public function setMark(string $mark): self
    {
        $this->mark = $mark;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getFinishing(): ?string
    {
        return $this->finishing;
    }

    public function setFinishing(string $finishing): self
    {
        $this->finishing = $finishing;

        return $this;
    }

    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    public function setFuel(string $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getGearbox(): ?string
    {
        return $this->gearbox;
    }

    public function setGearbox(string $gearbox): self
    {
        $this->gearbox = $gearbox;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNewCar(): ?bool
    {
        return $this->newCar;
    }

    public function setNewCar(bool $newCar): self
    {
        $this->newCar = $newCar;

        return $this;
    }

    public function getYearCar(): ?string
    {
        return $this->yearCar;
    }

    public function setYearCar(string $yearCar): self
    {
        $this->yearCar = $yearCar;

        return $this;
    }

    public function getMileage(): ?string
    {
        return $this->mileage;
    }

    public function setMileage(string $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?string
    {
        return $this->stock;
    }

    public function setStock(string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrderId(): Collection
    {
        return $this->orderId;
    }

    public function addOrderId(Order $orderId): self
    {
        if (!$this->orderId->contains($orderId)) {
            $this->orderId[] = $orderId;
            $orderId->addCarsId($this);
        }

        return $this;
    }

    public function removeOrderId(Order $orderId): self
    {
        if ($this->orderId->removeElement($orderId)) {
            $orderId->removeCarsId($this);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function setImageFile(File $image = null): Car
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFileUrl(): ?string
    {
        return $this->fileUrl;
    }

    public function setFileUrl(?string $fileUrl): Car
    {
        $this->fileUrl = $fileUrl;

        return $this;
    }
}
