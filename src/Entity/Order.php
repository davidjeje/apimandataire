<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Action\NotFoundAction;
use Symfony\Component\Validator\Constraints as Assert; 

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 * @ApiResource(
 *      attributes = {
 *          "pagination_items_per_page" = 10,
 *          "security" = "is_granted('ROLE_ADMIN')"
 *      },
 *      normalizationContext={"groups"={"order:read"}},
 *      denormalizationContext={"groups"={"order:write"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context" = {"groups"={"order:item"}},
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              }
 *          },
 *          "post"={
 *              "controller" = ApiPlatform\Core\Action\NotFoundAction::class,
 *              "openapi_context" = {"summary" = "hidden"},
 *              "read" = false,
 *              "output" = false,
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              }
 *          },
 *          "create_order"={
 *              "security" = "is_granted('ROLE_USER')",
 *              "method"="POST",
 *              "path"="/orders/create",
 *              "controller"=App\Controller\CreateOrderController::class,
 *              "normalization_context" = {"groups"={"order:item"}},
 *              "openapi_context" = {
 *                  "security" = {{"cookieAuth" = {}}},
 *                  "summary" = "Permet d'attribuer la création d'une commande à un Utilisateur connecté.",
 *                  "requestBody" = {
 *                      "content" = {
 *                          "application/json" = {
 *                              "schema" = {
 *                                  "type" = "object",
 *                                  "properties" = {
 *                                      "date" = {"type" = "string"},
 *                                      "numberCars" = {"type" = "integer"},
 *                                      "carsId" = {"type" = "string"},
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          },
 *          "my_orders"={
 *              "security" = "is_granted('ROLE_USER')", 
 *              "method"="GET",
 *              "path"="/orders/my",
 *              "read" = false,
 *              "controller"=App\Controller\MyOrderController::class,
 *              "normalization_context" = {"groups"={"order:item"}},
 *              "openapi_context" = {
 *                  "security" = {{"cookieAuth" = {}}},
 *                  "summary" = "Permet à un Utilisateur connecté de voir les commandes qui la faite.",
 *                  
 *              }
 *          },
 *      },
 *      itemOperations={
 *          "get"={
 *              "controller" = ApiPlatform\Core\Action\NotFoundAction::class,
 *              "openapi_context" = {"summary" = "hidden"},
 *              "read" = false,
 *              "output" = false,
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              }
 *          },
 *          "put"={
 *              "validation_groups" = {"validation"},
 *              "security" = "is_granted('ORDERS_ME', object)",
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              }
 *          },
 *          "patch"={
 *              "denormalization_context" = {"groups"={"order:patch"}},
 *              "validation_groups" = {"validation"},
 *              "security" = "is_granted('ORDERS_ME', object)",
 *              "openapi_context" = {
 *                  "security" = {
 *                      {"cookieAuth" = {}}
 *                  }
 *              }
 *          },
 *          "delete"={
 *              "security" = "is_granted('ORDERS_ME', object)",
 *              "openapi_context" = {
 *                  "security" = {{"cookieAuth" = {}}} 
 *              },
 *          },
 *      }
 * )
 */
class Order implements UserOwnerInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"order:read", "order:item"})
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Groups({"order:read", "order:write", "order:item", "order:patch"})
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order:item", "order:write", "order:patch"})
     */
    private $numberCars;

    /**
     * @ORM\ManyToMany(targetEntity=Car::class, inversedBy="orderId", cascade={"persist"})
     * @Groups({"order:item", "order:write"})
     */
    private $carsId;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orderId", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"order:item"})
     */
    private $user;

    public function __construct()
    {
        $this->carsId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNumberCars(): ?int
    {
        return $this->numberCars;
    }

    public function setNumberCars(int $numberCars): self
    {
        $this->numberCars = $numberCars;

        return $this;
    }

    /**
     * @return Collection|Car[]
     */
    public function getCarsId(): Collection
    {
        return $this->carsId;
    }

    public function addCarsId(Car $carsId): self
    {
        if (!$this->carsId->contains($carsId)) {
            $this->carsId[] = $carsId;
        }

        return $this;
    } 

    public function removeCarsId(Car $carsId): self
    {
        $this->carsId->removeElement($carsId);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
