<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use App\Entity\Order;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class OrderVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, ['ORDERS_ME'])) { 
            return false;
        }

        // only vote on `Order` objects
        if (!$subject instanceof Order) {
            return false;
        }

        return true;  
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }
        
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'ORDERS_ME':
                if ($user === $subject->getUser() || $this->security->isGranted('ROLE_ADMIN')) {
                        return true;
                }
                break;
        }

        return false;
    }
}
