<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Car;

class UserUnitTest extends TestCase
{
    public function testIsTrue() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		//$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(true);
		//dd($user->getRoles() === ['ROLE_USER']);
		$this->assertTrue($user->getEmail() === 'test@gmail.com');
		//$this->assertTrue($user->getRoles() === ['ROLE_USER']);
        $this->assertTrue($user->getPassword() === 'testUnit');
        $this->assertTrue($user->getName() === 'test');
        $this->assertTrue($user->getNumber() === '0652654896');
        $this->assertTrue($user->getAdress() === '2 rue du petit canal');
        $this->assertTrue($user->getCity() === 'Paris');
        $this->assertTrue($user->getZipCode() === '75011');
        $this->assertTrue($user->getIsVerified() === true);				
	}

    public function testIsFalse() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(true);

		$this->assertFalse($user->getEmail() === 'test1@gmail.com');
		$this->assertFalse($user->getRoles() === ['ROLE_USER2']);
        $this->assertFalse($user->getPassword() === 'testUnit2');
        $this->assertFalse($user->getName() === 'test2');
        $this->assertFalse($user->getNumber() === '0652654895');
        $this->assertFalse($user->getAdress() === '2 rue du petit canale');
        $this->assertFalse($user->getCity() === 'Parise');
        $this->assertFalse($user->getZipCode() === '75012');
        $this->assertFalse($user->getIsVerified() === false);				
	}

    public function testIsEmpty()
	{
		$user = new User();

		$this->assertEmpty($user->getId());
		$this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getName());
        $this->assertEmpty($user->getNumber());	
        $this->assertEmpty($user->getAdress());
        $this->assertEmpty($user->getCity());
        $this->assertEmpty($user->getZipCode());
        $this->assertEmpty($user->getIsVerified());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getUsername());
	}

    public function testAddGetRemoveCar()
	{
        $car = new Car();
        $user = new User();

        $this->assertEmpty($user->getCar());

        $user->addCar($car);
        $this->assertContains($car,$user->getCar());

        $user->removeCar($car);
		$this->assertEmpty($user->getCar());
	}

    public function testAddGetRemoveOrder()
	{
        $order = new Order();
        $user = new User();

        $this->assertEmpty($user->getOrderId());

        $user->addOrderId($order);
        $this->assertContains($order,$user->getOrderId());

        $user->removeOrderId($order);
		$this->assertEmpty($user->getCar());
	}
}
