<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Car;

class CarUnitTest extends TestCase
{
    public function testIsTrue() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(1);

		$date = new \dateTime();

        $car = new Car();
		$car->setMark('peugeot');
		$car->setModel('308');
		$car->setFinishing('Feline');
		$car->setFuel('essence');
		$car->setGearbox('automatique');
        $car->setDescription('Tout option');
        $car->setNewCar(true);
        $car->setYearCar('2022');
        $car->setMileage('1klm');
        $car->setColor('noir');
        $car->setPrice('30 000');
        $car->setStock('2');
		$car->setUser($user);
		$car->setFilePath('public/uploads/images/cars');
		$car->setUpdatedAt($date);

		
		$this->assertTrue($car->getMark() === 'peugeot');
		$this->assertTrue($car->getModel() === '308');
        $this->assertTrue($car->getFinishing() === 'Feline');
		$this->assertTrue($car->getFuel() === 'essence');
		$this->assertTrue($car->getGearbox() === 'automatique');
		$this->assertTrue($car->getDescription() === 'Tout option');
		$this->assertTrue($car->getNewCar() === true);
		$this->assertTrue($car->getYearCar() === '2022');
		$this->assertTrue($car->getMileage() === '1klm');
		$this->assertTrue($car->getColor() === 'noir');
		$this->assertTrue($car->getPrice() === '30 000');
		$this->assertTrue($car->getStock() === '2');
		$this->assertTrue($car->getUser() === $user);
		$this->assertTrue($car->getFilePath() === 'public/uploads/images/cars');
		$this->assertTrue($car->getUpdatedAt() === $date);				
	}

    public function testIsFalse() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(1);

		$user2 = new User();
		$user2->setEmail('test2@gmail.com');
		$user2->setRoles(['ROLE_USER']);
		$user2->setPassword('testUnit2');
		$user2->setName('test2');
		$user2->setNumber('0652654896');
        $user2->setAdress('2 rue du petit canal');
        $user2->setCity('Paris');
        $user2->setZipCode('75011');
        $user2->setIsVerified(false);

		$date = new \dateTime();
		$date2 = new \DateTime('2022-01-24');

        $car = new Car();
		$car->setMark('peugeot');
		$car->setModel('308');
		$car->setFinishing('Feline');
		$car->setFuel('essence');
		$car->setGearbox('automatique');
        $car->setDescription('Tout option');
        $car->setNewCar(true);
        $car->setYearCar('2022');
        $car->setMileage('1klm');
        $car->setColor('noir');
        $car->setPrice('30 000');
        $car->setStock('2');
		$car->setUser($user);
		$car->setFilePath('public/uploads/images/cars');
		$car->setUpdatedAt($date);

		
		$this->assertFalse($car->getMark() === 'peugeot2');
		$this->assertFalse($car->getModel() === '3082');
        $this->assertFalse($car->getFinishing() === 'Feline2');
		$this->assertFalse($car->getFuel() === 'essence2');
		$this->assertFalse($car->getGearbox() === 'automatique2');
		$this->assertFalse($car->getDescription() === 'Tout option2');
		$this->assertFalse($car->getNewCar() === false);
		$this->assertFalse($car->getYearCar() === '2023');
		$this->assertFalse($car->getMileage() === '2klm');
		$this->assertFalse($car->getColor() === 'noir2');
		$this->assertFalse($car->getPrice() === '30 0002');
		$this->assertFalse($car->getStock() === '22');
		$this->assertFalse($car->getUser() === $user2);
		$this->assertFalse($car->getFilePath() === 'public/uploads/images/cars2');
		$this->assertFalse($car->getUpdatedAt() === $date2);			
	}

    public function testIsEmpty()
	{
		$car = new Car();

		$this->assertEmpty($car->getId());
		$this->assertEmpty($car->getMark());
        $this->assertEmpty($car->getModel());
		$this->assertEmpty($car->getFinishing());
        $this->assertEmpty($car->getFuel());
		$this->assertEmpty($car->getGearbox());
		$this->assertEmpty($car->getDescription());
		$this->assertEmpty($car->getNewCar());
		$this->assertEmpty($car->getYearCar());
		$this->assertEmpty($car->getMileage());
		$this->assertEmpty($car->getColor());
		$this->assertEmpty($car->getPrice());
		$this->assertEmpty($car->getStock());
		$this->assertEmpty($car->getUser());
		$this->assertEmpty($car->getFilePath());
		$this->assertEmpty($car->getUpdatedAt());
		$this->assertEmpty($car->getImageFile());	
	}

    public function testAddGetRemoveOrder()
	{
        $order = new Order();
		$car = new Car();

        $this->assertEmpty($car->getOrderId());

        $car->addOrderId($order);
        $this->assertContains($order,$car->getOrderId());

        $car->removeOrderId($order);
		$this->assertEmpty($car->getOrderId());
	}
}
