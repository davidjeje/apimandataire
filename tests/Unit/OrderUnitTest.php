<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Car;

class OrderUnitTest extends TestCase
{
    public function testIsTrue() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(1);

        $date = new \dateTime();
		$order = new Order();

		$order->setDate($date);
		$order->setNumberCars(1);
        $order->setUser($user);
		
		$this->assertTrue($order->getDate() === $date);
		$this->assertTrue($order->getNumberCars() === 1);
        $this->assertTrue($order->getUser() === $user);				
	}

    public function testIsFalse() 
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setNumber('0652654896');
        $user->setAdress('2 rue du petit canal');
        $user->setCity('Paris');
        $user->setZipCode('75011');
        $user->setIsVerified(1);

        $user2 = new User();
		$user2->setEmail('test2@gmail.com');
		$user2->setRoles(['ROLE_USER']);
		$user2->setPassword('testUnit2');
		$user2->setName('test2');
		$user2->setNumber('0652654896');
        $user2->setAdress('2 rue du petit canal');
        $user2->setCity('Paris');
        $user2->setZipCode('75011');
        $user2->setIsVerified(0);

		
        $date = new \dateTime();
        $date2 = new \DateTime('2022-01-24');
		$order = new Order();

		$order->setDate($date);
		$order->setNumberCars(1);
        $order->setUser($user);
		
		$this->assertFalse($order->getDate() === $date2);
		$this->assertFalse($order->getNumberCars() === 2);
        $this->assertFalse($order->getUser() === $user2);				
	}

    public function testIsEmpty()
	{
		$order = new Order();

		$this->assertEmpty($order->getId());
		$this->assertEmpty($order->getDate());
        $this->assertEmpty($order->getNumberCars());
		$this->assertEmpty($order->getUser());	
	}

    public function testAddGetRemoveCar()
	{
        $car = new Car();
        $order = new Order();

        $this->assertEmpty($order->getCarsId());

        $order->addCarsId($car);
        $this->assertContains($car,$order->getCarsId());

        $order->removeCarsId($car);
		$this->assertEmpty($order->getCarsId());
	}
}
